/*
 * Copyright (C) 2015 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Michi Henning <michi.henning@canonical.com>
 */

#pragma once

#include <boost/optional.hpp>
// std::optional doesn't support operator<< so core::Optional users shouldn't
// expect it to be available. However, consumers (e.g. GoogleTest) can
// auto-detect this operator using "ADL", so without including
// boost/optional/optional_io.hpp they will detect the non-working one which
// breaks the build.
#include <boost/optional/optional_io.hpp>

namespace core
{

/**
\brief Convenience typedef for nullable values.

\note You should use `core::Optional` in preference to `boost::optional`
in your code. This will ease an eventual transition to `std::optional`.
*/
template <typename T>
using Optional = boost::optional<T>;

}  // namespace core
